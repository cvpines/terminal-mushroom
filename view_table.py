import sys
import terminalmushroom


def _key_to_display_name(key, table):
    identifier_or_contents, children = table[key]
    if not children:
        # This is a terminal node
        identifier_or_contents = repr(identifier_or_contents)
    return identifier_or_contents


def cli():
    if len(sys.argv) != 2:
        print('Usage: view_tree input-file')
        return

    in_file = sys.argv[1]
    with open(in_file, 'rb') as f:
        raw = f.read()

    index, table = terminalmushroom.deserialize_tree_table(raw)

    print('Index: {index!r}'.format(index=index))
    print('=' * 79)

    keys = sorted(table.keys())
    for key in keys:

        identifier_or_contents, children = table[key]

        if children:
            # Nonterminal node
            children_display = ', '.join(str(child) for child in children)
            print('{key}: {identifier}({children})'.format(
                key=key, identifier=identifier_or_contents, children=children_display))
        else:
            # Terminal node
            print('{key}: {contents!r}'.format(
                key=key, contents=identifier_or_contents))

        if key + 1 in index:
            print('-' * 79)


if __name__ == '__main__':
    cli()
