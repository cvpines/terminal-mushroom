import sys
import terminalmushroom


def _key_to_display_name(key, table):
    identifier_or_contents, children = table[key]
    if not children:
        # This is a terminal node
        identifier_or_contents = repr(identifier_or_contents)
    return identifier_or_contents


def cli():
    if len(sys.argv) != 2:
        print('Usage: view_tree input-file')
        return

    in_file = sys.argv[1]
    with open(in_file, 'rb') as f:
        raw = f.read()

    index, table = terminalmushroom.deserialize_tree_table(raw)
    trees = terminalmushroom.build_trees_from_table(index, table)

    for tree in trees:
        print(tree.flatten())


if __name__ == '__main__':
    cli()
